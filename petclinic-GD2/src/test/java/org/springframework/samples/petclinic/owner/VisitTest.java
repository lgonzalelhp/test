package org.springframework.samples.petclinic.owner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.samples.petclinic.vet.Vet;
import org.springframework.samples.petclinic.visit.Visit;

public class VisitTest {
	
	public static Visit visit;
	public static Vet vet;
	
	@BeforeClass
	//This method is called before executing this suite
	public static void initClass() {
		
		if (vet == null) {
			vet = new Vet();
			vet.setId(1);
			vet.setFirstName("Roberto");
			vet.setLastName("Rodriguez");
		}
		
		if (visit == null) {
			visit = new Visit();
			visit.setDate(LocalDate.now());
			visit.setDescription("Descripcion");
			visit.setVet(vet);
		}
		
	}
	
	@Test
	public void testVet() {
		Vet vet2 = visit.getVet();
		assertNotNull(visit.getVet());
		assertNotNull(vet2);
		assertEquals(vet.getFirstName(), vet2.getFirstName());
		assertEquals(vet.getId(), vet2.getId());
		assertEquals(vet.getLastName(), vet2.getLastName());
	}
	
	@After
	public void finish() {
		this.visit = null;
		this.vet = null;
	}

}
