package org.springframework.samples.petclinic.owner;

import java.time.LocalDate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.samples.petclinic.vet.Specialty;
import org.springframework.samples.petclinic.vet.Vet;
import org.springframework.samples.petclinic.vet.VetRepository;
import org.springframework.samples.petclinic.visit.Visit;
import org.springframework.samples.petclinic.visit.VisitRepository;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Repository;

@RunWith(SpringRunner.class)
@DataJpaTest(includeFilters = @ComponentScan.Filter(Repository.class))
public class VisitRepositoryTest {
	
	@Autowired
	private VisitRepository  visits;
	
	private Visit visit1, visit2;
	private Vet vet1;
	private Pet pet1, pet2;
	private Specialty spe1;
	private Owner own1;
	private PetType type1;
	

	@Before
	public void init() {
		
		if (this.spe1 == null) {
			spe1 = new Specialty();
			spe1.setId(1);
			spe1.setName("Especialidad1");
		}
		
		if (this.type1 == null) {
			type1 = new PetType();
		    type1.setId(1);
		    type1.setName("Canino");
		}
	
		if (this.own1 == null) {
			own1 = new Owner();
	        own1.setFirstName("Roberto");
	        own1.setLastName("Rodriguez");
	        own1.setAddress("Calle");
	        own1.setCity("Caceres");
	        own1.setTelephone("666777888");
	        own1.setId(1);
		}
	   
	    if (this.pet1 == null) {
	    	pet1 = new Pet();
		    pet1.setName("PetName1");
		    pet1.setBirthDate(LocalDate.now());
		    pet1.setComments("Comments 1");
		    pet1.setWeight(23);
		    pet1.setOwner(own1);
		    pet1.setType(type1);
		    pet1.setId(1);
	    }
	    
	    if(this.pet2 == null) {
	    	pet2 = new Pet();
	    	pet2.setName("PetName2");
	    	pet2.setBirthDate(LocalDate.now());
	    	pet2.setComments("Comments 2");
	    	pet2.setWeight(23);
	    	pet2.setOwner(own1);
	    	pet2.setType(type1);
	    	pet2.setId(2);
	    }
	    
	    if (this.vet1 == null) {
	    	vet1 = new Vet();
			vet1.setFirstName("Roberto");
			vet1.setLastName("Rodriguez");
			vet1.setHomeVisits(false);
			vet1.addSpecialty(spe1);
			vet1.setId(1);
	    }
        
	    if (this.visit1 == null) {
	    	visit1 = new Visit();
			visit1.setDescription("Descripcion 1");
			visit1.setDate(LocalDate.now());
			visit1.setVet(vet1);
			visit1.setPetId(pet1.getId());
			visits.save(visit1);
	    }
	    
	    if (this.visit2 == null) {
	    	visit2 = new Visit();
	    	visit2.setDescription("Descripcion 2");
	    	visit2.setDate(LocalDate.of(2018, 1, 1));
	    	visit2.setVet(vet1);
	    	visit2.setPetId(pet2.getId());
	    	visits.save(visit2);
	    }
		
	}
	
	@Test
	public void testFindById() {
		
		Visit visitFindById = this.visits.findById(visit1.getId());

		assertNotNull(visitFindById.getDescription());
		assertEquals(visitFindById.getDescription(), visit1.getDescription());

		
	}
	
	@Test
	public void testFindByIdNotEqual() {
		
		Visit visitFindById = this.visits.findById(visit2.getId());
					
		assertNotNull(visitFindById.getDate());
		assertNotEquals(visitFindById.getDate(), visit1.getDate());	
		
		assertNotNull(visitFindById.getDescription());		
		assertNotEquals(visitFindById.getDescription(), visit1.getDescription());	
					
	}
	
	@After
	public void finish() {
		this.visit1=null;
		this.visit2=null;
	}
	

}